/* Ответы на теоритические вопросы:
1. В JS существует 8 основных типов данных, а именно number, string, boolean, bigint,
null, undefined, symbol, object.
2. Оператор строгого равенства === проверяет равенство без приведения типов к общему.
Оператор нестрогого равенства ==. Перед сравнением приводит величины к общему типу.
3. Оператор - это символ, с помощью которого выполняются различные задачи
(суммирование, деление и тд.) */


let personName = prompt('Введите Ваше имя');
let personAge = Number(prompt('Введите свой возраст'));
let verify;

if (personAge < 18) {
    alert('You are not allowed to visit this website');
}
else if (personAge >= 18 && personAge <= 22) {
    verify = confirm('Are you sure you want to continue?');
    if (verify) {
        alert(`Welcome, ${personName}`);
    } else {
        alert('You are not allowed to visit this website');
    }
} else if (personAge >= 23) {
    alert(`Welcome, ${personName}`);
}

while (!personName || /\d/g.test(personName) || (!personAge || isNaN(personAge))) {
    if (!personName) {
        personName = prompt('Вы еще не ввели имя, введите повторно!');
    }

    if (/\d/g.test(personName)) {
        personName = prompt('Ваше имя введено некорректно, повторите ввод', personName);
    }

    if (!personAge) {
        personAge = Number(prompt('Вы еще не ввели возраст, введите повторно!'));
    }

    if (isNaN(personAge)) {
        personAge = Number(prompt('Ваш возраст введен некорректно, повторите ввод!', personAge));
    }
}

